namespace Password {
    [GtkTemplate (ui = "/org/emilien/Password/ui/menuTheme.ui")]
    public class Widgets.Menu : Gtk.Box {
        private const GLib.ActionEntry[] ACTION_ENTRIES = {
            { "profils", on_profil,"s"},
            { "nouveauProfil", nouveauProfil}
        };

        [GtkChild]
        public unowned Gtk.CheckButton follow;
        [GtkChild]
        public unowned Gtk.CheckButton light;
        [GtkChild]
        public unowned Gtk.CheckButton dark;

        private GLib.Menu sousMenu = new GLib.Menu();

        public Window win {get; set;}
        public Calculateur calculateur {get; set;}
        public Generateur generateur {get; set;}

        //public Menu (Window win, Reglages reglages) {
        public Menu (Window win, Calculateur calculateur, Generateur generateur) {
            Object (
                win: win,
                calculateur: calculateur,
                generateur: generateur
            );
            win.add_action_entries(ACTION_ENTRIES, this);

            follow.notify["active"].connect(() => {changeTheme(true);});
            light.notify["active"].connect(() => {changeTheme(true);});
            dark.notify["active"].connect(() => {changeTheme(true);});
        }

        construct {

        }

        private void creationMenu(Gtk.MenuButton boutonMenu, List<string> listeProfils) {

            GLib.Menu menu = new GLib.Menu();

            var builder = new Gtk.Builder.from_resource ("/org/emilien/Password/ui/menu.ui");
            boutonMenu.menu_model = (MenuModel)builder.get_object ("menu");

            menu = (GLib.Menu)boutonMenu.get_menu_model();
            GLib.Menu section = new GLib.Menu();

            if(win.nbProfils>0){
                for(int i=0; i<win.nbProfils; i++){
                    GLib.MenuItem item = new MenuItem(listeProfils.nth_data((uint) i),"win.profils");
                    item.set_attribute ("target","s",listeProfils.nth_data((uint) i));
                    sousMenu.append_item (item);
                }
            }

            GLib.MenuItem item = new MenuItem(_("Add a new profile..."),"win.nouveauProfil");
            sousMenu.append_item (item);
            section.append_submenu(_("Profiles"),sousMenu);
            menu.insert_section (1,null, section);

            boutonMenu.menu_model = menu;
        }

        private void modificationMenu(Gtk.MenuButton boutonMenu, List<string> listeProfils) {

            sousMenu.remove_all();
            if(win.nbProfils>0){
                for(int i=0; i<win.nbProfils; i++){
                    GLib.MenuItem item = new MenuItem(listeProfils.nth_data((uint) i),"win.profils");
                    item.set_attribute ("target","s",listeProfils.nth_data((uint) i));
                    sousMenu.append_item (item);
                }
            }

            GLib.MenuItem item = new MenuItem(_("Add a new profile..."),"win.nouveauProfil");
            sousMenu.append_item (item);
        }

        //THEME

        public void changeTheme(bool ecriture)
        {
            string theme ="";

            if(follow.get_active ()) { Adw.StyleManager.get_default ().color_scheme = DEFAULT; theme = "DEFAULT";};
            if(light.get_active ()) { Adw.StyleManager.get_default ().color_scheme = FORCE_LIGHT; theme = "FORCE_LIGHT";};
            if(dark.get_active ()) { Adw.StyleManager.get_default ().color_scheme = FORCE_DARK; theme = "FORCE_DARK";};

            if(ecriture){
                var dis = FileStream.open (win.dossierConfig + "/theme.conf", "w");
                dis.write(theme.data);
            }
        }

        //PROFILS

        public void majListeProfils(bool creation)
        {
            //NETTOYAGE LISTE
            win.listeProfils.foreach ((entry) => win.listeProfils.remove (entry));

            //RECHERCHE DES PROFILS
            try{
                Dir dir = Dir.open (win.dossierConfig);
                string? name = null;
                while((name = dir.read_name()) != null){
                    if(name.substring(0,9)=="password-" && name.substring(name.char_count()-5,5)==".conf"){
                        win.listeProfils.append(name.substring(9,name.char_count()-14));
                    }
                }
            } catch (FileError err){}

            win.nbProfils = (int) win.listeProfils.length();

            if(creation) creationMenu(win.boutonMenu,win.listeProfils);
            if(!creation) modificationMenu(win.boutonMenu,win.listeProfils);

            var pop = (Gtk.PopoverMenu)win.boutonMenu.get_popover ();
            pop.add_child (this, "theme");
        }

        public void on_profil(GLib.SimpleAction action, GLib.Variant? param)
        {
            string profil = param.get_string();
            chargementProfil(profil);
        }

        public void chargementProfil(string profil)
        {
            calculateur.labelProfil.hide();
            generateur.labelProfil.hide();
            calculateur.labelProfil.set_text("");
            generateur.labelProfil.set_text("");
            win.profilActuel = profil;
            if(win.profilActuel!="") {
                calculateur.labelProfil.show();
                generateur.labelProfil.show();
                calculateur.labelProfil.set_text(_("Profile: ") + profil);
                generateur.labelProfil.set_text(_("Profile: ") + profil);
            }
            if(win.codeVerrouillage == "") lectureFichierConfig(win.dossierConfig, win.profilActuel);
            if(win.codeVerrouillage != "") lectureFichierCryptConfig(win.dossierConfig, win.profilActuel, win.codeVerrouillage);
            //PRE-REMPLISSAGE ALIAS
            calculateur.texteAlias.set_text(parametres[3].get_string());

            //ÉCRITURE FICHIER DERNIER PROFIL CHARGÉ
            var dis = FileStream.open (win.dossierConfig + "/profile.conf", "w");
            dis.write(profil.data);
        }

        private void nouveauProfil() {

            var main_window = (Gtk.Window) win.get_root();
            Adw.MessageDialog dialog = new Adw.MessageDialog(main_window, _("Enter a name for the new profile:"), "");

            dialog.close_response = "cancel";
            dialog.add_response("cancel", _("Cancel"));
            dialog.add_response("create", _("Create"));
            dialog.set_response_appearance("cancel", Adw.ResponseAppearance.DESTRUCTIVE);
            dialog.set_response_appearance("create", Adw.ResponseAppearance.SUGGESTED);

            Gtk.Entry nom = new Gtk.Entry();
            dialog.set_extra_child(nom);

            nom.activate.connect(() => {if(nom.get_text()!=""){
                dialog.close();
                creationProfil(nom.get_text());
                }
            });

            dialog.response.connect((response) => {
                if(response=="create" && nom.get_text()!="") {
                    creationProfil(nom.get_text());
                }
            });

            dialog.present();
        }

        private void creationProfil(string nomProfil) {

            win.profilActuel = nomProfil;
            if(win.codeVerrouillage == "") ecritureFichierConfig(win.profilActuel, win.dossierConfig);
            if(win.codeVerrouillage != "") ecritureFichierCryptConfig(win.profilActuel, win.dossierConfig, win.codeVerrouillage);
            majListeProfils(false);
            calculateur.labelProfil.show();
            generateur.labelProfil.show();
            calculateur.labelProfil.set_text(_("Profile: ") + win.profilActuel);
            generateur.labelProfil.set_text(_("Profile: ") + win.profilActuel);
            //ÉCRITURE FICHIER DERNIER PROFIL CHARGÉ
            var disProfil = FileStream.open (win.dossierConfig + "/profile.conf", "w");
            disProfil.write(win.profilActuel.data);
        }
    }
}
