namespace Password {
    [GtkTemplate (ui = "/org/emilien/Password/ui/pageCalculateur.ui")]
    public class Calculateur : Gtk.Box {
        [GtkChild]
        public unowned Gtk.Entry texteAlias;
        [GtkChild]
        public unowned Gtk.Entry texteSecret;
        [GtkChild]
        public unowned Gtk.Label motDePasse;
        [GtkChild]
        public unowned Gtk.Label labelFaille;
        [GtkChild]
        public unowned Gtk.Label labelProfil;

        public Window win {get; set;}

        public Generateur generateur {get; set;}

        public Calculateur (Window win, Generateur generateur) {
            Object (
                win: win,
                generateur: generateur
            );

            texteAlias.changed.connect(texteChange);
            texteSecret.changed.connect(texteChange);
            texteAlias.activate.connect(() => {texteSecret.grab_focus();});
            texteSecret.activate.connect(entreTexteSecret);
            texteAlias.icon_press.connect(() => {texteAlias.set_text("");});
            texteSecret.icon_press.connect(() => {texteSecret.set_text("");});
        }

        construct {

            texteAlias.set_alignment((float) 0.5);
            texteSecret.set_alignment((float) 0.5);
            unichar ch ='●';
            texteAlias.set_invisible_char(ch);
        }

        public void calcul() {

            string[] mdp = mdpCalcul(texteAlias.get_text(), texteSecret.get_text(), settings.get_boolean("masquage"),//
                parametres[0].get_int32(), parametres[1].get_boolean(), parametres[4].get_int32(),//
                parametres[2].get_string(), parametres[5].get_boolean(), parametres[6].get_boolean(),//
                parametres[7].get_boolean(), parametres[9].get_boolean(), parametres[8].get_boolean(), false);
            motDePasse.set_label(mdp[1]);
            win.passwordCourt = mdp[0];
            win.revealCopier.set_reveal_child(true);
            if(settings.get_boolean("enablepwn") && settings.get_boolean("pwn")) GLib.Timeout.add (100, () => {majFaille();return false;});
        }

        public void texteChange() {
            labelFaille.set_label("");
             if(texteAlias.get_text()!="" || texteSecret.get_text()!=""){ //SI AU MOINS UN DES DEUX REMPLI
                   if(texteSecret.get_text()!="") win.boutonNettoyer.set_icon_name("user-trash-full-symbolic");
                   if(texteAlias.get_text()!="" && texteAlias.get_icon_name(SECONDARY)==null) texteAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
                   if(texteSecret.get_text()!="" && texteSecret.get_icon_name(SECONDARY)==null) texteSecret.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
               }
             if(texteAlias.get_text()=="" || texteSecret.get_text()==""){ //SI AU MOINS UN DES DEUX VIDES
                    motDePasse.set_label(_("your password"));
                    win.passwordCourt="";
                    win.revealCopier.set_reveal_child(false);
                    if(texteAlias.get_text()=="" && texteAlias.get_icon_name(SECONDARY)!=null) texteAlias.set_icon_from_icon_name(SECONDARY,null);
                    if(texteSecret.get_text()=="" && texteSecret.get_icon_name(SECONDARY)!=null) texteSecret.set_icon_from_icon_name(SECONDARY,null);
                    }

             if(texteAlias.get_text()=="" && texteSecret.get_text()==""){ //SI LES DEUX VIDES
                   motDePasse.set_label(_("your password"));
                   win.passwordCourt="";
                   win.revealCopier.set_reveal_child(false);
               }
             if(texteAlias.get_text()!="" && texteSecret.get_text()!=""){ //SI LES DEUX REMPLIS
                    calcul();
               }

             if(generateur.motDePasseAleatoire.get_label()=="" && texteSecret.get_text()=="") win.boutonNettoyer.set_icon_name("user-trash-symbolic");

        }

        public void entreTexteSecret() {
            if(win.passwordCourt!=""){
                if(settings.get_boolean("quitter")){
                    win.quitterAppli=true;
                    win.attendreAvanDeFermer=true;
                }
                //COPIE MOT DE PASSE
                win.copier();
            }
        }

        public void majFaille() {
            //HAVEIBEENPWNED
            int faille = appelCheckPwned(win.passwordCourt);
            if(faille > 0){
                labelFaille.set_label(_("Be careful!\nThis password appears ")+ faille.to_string() + _(" times in HaveIBeenPwned database"));
                uint8[] rouge = "* { color: #F53C32; }".data;
                var p1 = new Gtk.CssProvider();
                try{p1.load_from_data(rouge);} catch (Error err) {}
                labelFaille.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
            else if(faille == 0){
                labelFaille.set_label(_("This password does not appear in HaveIBeenPwned database"));
                uint8[] vert = "* { color: #00AB63; }".data;
                var p1 = new Gtk.CssProvider();
                try{p1.load_from_data(vert);} catch (Error err) {}
                labelFaille.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
            else{//FAILLE = -1 : PAS DE CONNEXION INTERNET
                labelFaille.set_label(_("Check your internet connection!"));
                uint8[] rouge = "* { color: #F53C32; }".data;
                var p1 = new Gtk.CssProvider();
                try{p1.load_from_data(rouge);} catch (Error err) {}
                labelFaille.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
        }
    }
}

