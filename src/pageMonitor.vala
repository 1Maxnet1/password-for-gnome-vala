namespace Password {
    [GtkTemplate (ui = "/org/emilien/Password/ui/pageMonitor.ui")]
    public class Monitor : Gtk.Box {
        [GtkChild]
        public unowned Gtk.Entry passPwned;
        [GtkChild]
        public unowned Gtk.Button boutonPwned2;
        [GtkChild]
        public unowned Gtk.Label labelFaillePwned;
        [GtkChild]
        public unowned Gtk.Label labelFaillePwned2;
        [GtkChild]
        public unowned Gtk.Button boutonMonitor;

        public Window win {get; set;}

        private string url;

        public Monitor (Window win) {
            Object (
                win: win
            );

            url = "https://monitor.firefox.com/";

            passPwned.icon_press.connect(passPwnedChgt);
            boutonPwned2.clicked.connect(majFaillePwned);
            passPwned.activate.connect(majFaillePwned);
            passPwned.changed.connect(() => {labelFaillePwned.set_text("");labelFaillePwned2.hide();});
            boutonMonitor.clicked.connect(lancementMonitor);
        }

        construct {
            unichar ch ='●';
            Pango.AttrList attrsEntrees = new Pango.AttrList ();
            attrsEntrees.insert (Pango.attr_family_new ("Monospace"));
            passPwned.set_attributes(attrsEntrees);
            passPwned.set_invisible_char(ch);
            passPwned.set_icon_from_icon_name(SECONDARY,"view-conceal-symbolic");
            labelFaillePwned2.hide();
        }

        public void passPwnedChgt() {

            if(passPwned.get_icon_name(SECONDARY)=="view-conceal-symbolic"){
                passPwned.set_icon_from_icon_name(SECONDARY,"view-reveal-symbolic");
                passPwned.set_visibility(false);
            }
            else{
                passPwned.set_icon_from_icon_name(SECONDARY,"view-conceal-symbolic");
                passPwned.set_visibility(true);
            }
        }

        public void majFaillePwned()
        {
             labelFaillePwned.set_label("");
             labelFaillePwned2.hide();
             if(passPwned.get_text()!=""){
                 int faille = appelCheckPwned(passPwned.get_text());
                 if(faille > 0){
                    labelFaillePwned.set_label(_("Be careful!\nThis password appears ")+ faille.to_string() + _(" times in HaveIBeenPwned database"));
                    uint8[] rouge = "* { color: #F53C32; }".data;
                    var p1 = new Gtk.CssProvider();
                    try{p1.load_from_data(rouge);} catch (Error err) {}
                    labelFaillePwned.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    labelFaillePwned2.show();
                }
                else if(faille==0){
                    labelFaillePwned.set_label(_("This password does not appear in HaveIBeenPwned database"));
                    uint8[] vert = "* { color: #00AB63; }".data;
                    var p1 = new Gtk.CssProvider();
                    try{p1.load_from_data(vert);} catch (Error err) {}
                    labelFaillePwned.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                }
                else{//FAILLE = -1 : PAS DE CONNEXION INTERNET
                    labelFaillePwned.set_label(_("Check your internet connection!"));
                    uint8[] rouge = "* { color: #F53C32; }".data;
                    var p1 = new Gtk.CssProvider();
                    try{p1.load_from_data(rouge);} catch (Error err) {}
                    labelFaillePwned.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                 }
             }
        }

        private void lancementMonitor() {

            Gtk.show_uri(null, url, Gdk.CURRENT_TIME); //PROVISOIRE // EN ATTENTE DE WEBKITGTK6
        }
    }
}

