namespace Password{

    public int calculForce(int length, bool remLower, bool remUpper, bool remNum, bool remSpec, bool onlyPin){

        int force = 0;
        int N=0;
        if(!remLower) N += 26;
        if(!remUpper) N += 26;
        if(!remNum) N += 10;
        if(!remSpec) N += 3;
        if(onlyPin) N = 10;

        for (int i=0;i <=128; i++) {
            if(Math.pow(2,i) < Math.pow(N,length)) force=i;
        }

        return force;
    }

    public void forceMotDePasse(Password.Reglages reglages)
        {
            int force = calculForce(parametres[4].get_int32(), parametres[5].get_boolean(), parametres[6].get_boolean(), parametres[7].get_boolean(), parametres[9].get_boolean(), parametres[8].get_boolean());
            //int force = calculForce((int) reglages.scaleLongueur.get_value(), reglages.switchMinuscules.get_state(), reglages.switchMajuscules.get_state(), reglages.switchChiffres.get_state(), reglages.switchSpeciaux.get_state(), reglages.switchPin.get_state());
            uint8[] rouge = "* { background: #F53C32; }".data;// #E90000; }";
            uint8[] orange = "* { background: #FFCD03; }".data;//#E99600; }";
            uint8[] bleu = "* { background: #00C2F0; }".data;
            uint8[] vert = "* { background: #00AB63; }".data;//#00D200; }";
            uint8[] transparent = "* { background: transparent; }".data;
            var p1 = new Gtk.CssProvider();
            var p2 = new Gtk.CssProvider();

            if(force < 64){
                reglages.iconeBouclier.set_from_icon_name("bouclier_0");
                reglages.labelForce.set_text(_("Very weak password") + " [" + force.to_string() + " bits]");
                try {
                    p1.load_from_data(rouge);
                    p2.load_from_data(transparent);
                    reglages.barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre1.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre2.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre3.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre4.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
            else if(force < 80){
                reglages.iconeBouclier.set_from_icon_name("bouclier_1");
                reglages.labelForce.set_text(_("Weak password") + " [" + force.to_string() + " bits]");
                try {
                    p1.load_from_data(orange);
                    p2.load_from_data(transparent);
                    reglages.barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre1.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre2.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre3.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre4.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
            else if(force < 100){
                reglages.iconeBouclier.set_from_icon_name("bouclier_2");
                reglages.labelForce.set_text(_("Fairly strong password") + " [" + force.to_string() + " bits]");
                try {
                    p1.load_from_data(bleu);
                    p2.load_from_data(transparent);
                    reglages.barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre1.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre2.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre3.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre4.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
            else if(force < 128){
                reglages.iconeBouclier.set_from_icon_name("bouclier_3");
                reglages.labelForce.set_text(_("Strong password") + " [" + force.to_string() + " bits]");
                try {
                    p1.load_from_data(vert);
                    p2.load_from_data(transparent);
                    reglages.barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre1.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre2.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre3.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre4.get_style_context().add_provider(p2, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
            else{
                reglages.iconeBouclier.set_from_icon_name("bouclier_4");
                reglages.labelForce.set_text(_("Very strong password") + " [≥ 128 bits]");
                try {
                    p1.load_from_data(vert);
                    reglages.barre0.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre1.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre2.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre3.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    reglages.barre4.get_style_context().add_provider(p1, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                    } catch (Error err) {
                }
            }
            reglages.iconeBouclier.set_icon_size(LARGE);
        }
}
