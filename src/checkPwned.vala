namespace Password{

    int appelCheckPwned(string mdpInit){
        int faille = 0;
        var loop = new MainLoop();
	    checkPwned.begin(mdpInit, (obj,res) => {
	        try{faille = checkPwned.end(res);}
	        catch(ThreadError e){}
	        loop.quit();
	    });
	    loop.run();
	    return faille;
    }

    async int checkPwned(string mdpInit) throws ThreadError{

        SourceFunc callback = checkPwned.callback;

        int faille = 0;

        ThreadFunc<bool> run = () => {
            string sha1mdp = GLib.Checksum.compute_for_string (ChecksumType.SHA1, mdpInit);
            string url = "https://api.pwnedpasswords.com/range/"+sha1mdp.substring(0,5);
            string chaine = "";
            int longueurMessage = 0;
            string ls_stderr;
	        int ls_status;
            Process.spawn_command_line_sync("curl "+url, out chaine, out ls_stderr, out ls_status);
            longueurMessage = chaine.length;

            if(longueurMessage!=0){
                bool stop = false;

                for(int i=0;i<chaine.length;i++){
                    if(chaine[i]==':' && !stop){
                        if(chaine.substring(i-35,35)==sha1mdp.substring(5).ascii_up()){
                            string[] temp = chaine.substring(i+1).split("\n");
                            faille = int.parse(temp[0]);
                            stop = true;
                        }
                    }
                }
            }
            else{
                //PROBLÈME CONNEXION
                faille = -1;
            }
            Idle.add((owned) callback);
            return true;
        };
        new Thread<bool>("checkPwned",run);
        yield;

        return faille;
    }
}
