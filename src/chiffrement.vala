namespace Password{

    private string chiffrement(string texte, string cle, int sens, bool varSwitch) {

        string resultat = "";
        if(texte!=""){
            if(varSwitch && sens == 1 && texte == "true") texte = "1";
            if(varSwitch && sens == 1 && texte == "false") texte = "0";
            for(int i=0;i<texte.char_count();i++){
                int32 crypt = (int32) sens*int.parse(cle);
                unichar ch = texte.get_char(texte.index_of_nth_char(i));
                ch += crypt;
                resultat += ch.to_string();
            }
            if(varSwitch && sens == -1 && resultat == "1") resultat = "true";
            if(varSwitch && sens == -1 && resultat == "0") resultat = "false";
        }
        return resultat;
    }

    private void chiffrementProfils(List<string> listeProfils, string profilActuel, string dossierConfig, string codeVerrouillage) {

        for(int i=0;i<listeProfils.length();i++){
            if(listeProfils.nth_data((uint) i) != profilActuel){
                string profile = listeProfils.nth_data((uint) i);
                FileUtils.rename(dossierConfig + "/password-" + profile +".conf", dossierConfig + "/tmp-password.conf");
                string fichierTemp = dossierConfig + "/tmp-password.conf";
                var dis = FileStream.open (fichierTemp, "r");
                string config = "";
                string cle = mdpCalcul(codeVerrouillage,codeVerrouillage, true, 4, true, 42 ,"", false , false , false , false , true, true)[0];
                config += chiffrement(dis.read_line(),cle.slice(0,2),1,false) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(2,4),1,false) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(2,3),1,false) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(3,4),1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(5,6),1,false) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(6,7),1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(7,8),1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(8,9),1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(9,10),1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(13,14),1,true) + "\n";
                var disProfil = FileStream.open (dossierConfig + "/password-" + profile +".conf", "w");
                disProfil.write(config.data);
                FileUtils.remove(dossierConfig + "/tmp-password.conf");
            }
        }
    }

    private void dechiffrementProfils(List<string> listeProfils, string profilActuel, string dossierConfig, string codeVerrouillage) {

        for(int i=0;i<listeProfils.length();i++){
            if(listeProfils.nth_data((uint) i) != profilActuel){
                string profile = listeProfils.nth_data((uint) i);
                FileUtils.rename(dossierConfig + "/password-" + profile +".conf", dossierConfig + "/tmp-password.conf");
                string fichierTemp = dossierConfig + "/tmp-password.conf";
                var dis = FileStream.open (fichierTemp, "r");
                string config = "";
                string cle = mdpCalcul(codeVerrouillage,codeVerrouillage, true, 4, true, 42 ,"", false , false , false , false , true, true)[0];
                config += chiffrement(dis.read_line(),cle.slice(0,2),-1,false) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(2,4),-1,false) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(2,3),-1,false) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(3,4),-1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(5,6),-1,false) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(6,7),-1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(7,8),-1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(8,9),-1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(9,10),-1,true) + "\n";
                config += chiffrement(dis.read_line(),cle.slice(13,14),-1,true) + "\n";
                var disProfil = FileStream.open (dossierConfig + "/password-" + profile +".conf", "w");
                disProfil.write(config.data);
                FileUtils.remove(dossierConfig + "/tmp-password.conf");
            }
        }
    }

    private void ecritureCle(string dossierConfig, string cle1, string cle2, string codeVerrouillage, string uuid) {

        string fichierCle = dossierConfig + "/cryptKey.conf";
        string config = "";
        //CREATION CLÉ DE CHIFFREMENT UNIQUE
        string cle = mdpCalcul(cle1,cle2, true, 4, true, 12 ,uuid, false , false , false , false , true, true)[0];
        for(int i=0;i<6;i++){
            int32 crypt = int.parse(cle.slice(2*i,2*i+2));
            unichar ch = codeVerrouillage.get_char(codeVerrouillage.index_of_nth_char(i));
            ch += crypt;
            config += ch.to_string();
        }
        config += "\n";
        var dis = FileStream.open (fichierCle, "w");
        dis.write(config.data);
    }
}
