namespace Password {
    [GtkTemplate (ui = "/org/emilien/Password/ui/pageWeb.ui")]
    public class Web : Gtk.Box {
        [GtkChild]
        public unowned Gtk.Box boiteWeb;

        public Window win {get; set;}
        public WebKit.WebView webView = new WebKit.WebView ();


        public Web (Window win) {
            Object (
                win: win
            );

            boiteWeb.append(webView);
            webView.load_uri ("https://qwant.com");
        }

        construct {

        }


    }
}

