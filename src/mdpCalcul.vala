namespace Password{

    const char base64_url_alphabet[] = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
    };

    private string passwordQMask;

    public string[] mdpCalcul(string mot1, string mot2, bool mask, int hash, bool conversion, int length, string saltString, bool remLower, bool remUpper, bool remNum, bool remSpec, bool onlyPin, bool config) {

        //08/11/21 : bool config : si true, on écrit pas la variable password car la routine est utilisée pour calculer la clé de chiffrement et non le mot de passe.

        string[] sortie = {null};


        if(mot1!="" && mot2!=""){

            string mot=mot1+mot2+saltString;
            string result = "";

            if(hash==0) result = GLib.Checksum.compute_for_string (ChecksumType.MD5, mot);
            if(hash==1) result = GLib.Checksum.compute_for_string (ChecksumType.SHA1, mot);
            if(hash==2) result = GLib.Checksum.compute_for_string (ChecksumType.SHA256, mot);
            if(hash==3) result = GLib.Checksum.compute_for_string (ChecksumType.SHA384, mot);
            if(hash==4) result = GLib.Checksum.compute_for_string (ChecksumType.SHA512, mot);
            if(hash==5) result = GLib.Hmac.compute_for_string (ChecksumType.MD5, {} , mot);
            if(hash==6) result = GLib.Hmac.compute_for_string (ChecksumType.SHA1, {} , mot);
            if(hash==7) result = GLib.Hmac.compute_for_string (ChecksumType.SHA256, {} , mot);
            if(hash==8) result = GLib.Hmac.compute_for_string (ChecksumType.SHA384, {} , mot);
            if(hash==9) result = GLib.Hmac.compute_for_string (ChecksumType.SHA512, {} , mot);

            //CONVERSION HEXA -> ASCII

            string newString = "";
            if(conversion){
                for(int i=0; i< result.length; i+=2){
                    string byte = result.substring(i,2);
                    char chr = (char) (int)long.parse(byte,16);
                    //char chr = (char) (int) byte.to_long(null,16); //DEPRECATED
                    newString+=chr.to_string ();
                }}
            else {
                newString = result;
            }

            //ENCODAGE BASE 64

            string passwordLong = GLib.Base64.encode(newString.data);
            string password = passwordLong.substring(0,length);

            //REMOVE NUMERIC

            if(remNum) {
                password = password.replace("0", "+");
                password = password.replace("1", "/");
                password = password.replace("2", "=");
                password = password.replace("3", "A");
                password = password.replace("4", "B");
                password = password.replace("5", "C");
                password = password.replace("6", "D");
                password = password.replace("7", "E");
                password = password.replace("8", "F");
                password = password.replace("9", "G");
            }

            //REMOVE SPECIAL

            if(remSpec) {
                password = password.replace("+", "A");
                password = password.replace("/", "B");
                password = password.replace("=", "B");//OUPS!!!
            }

            //REMOVE LOWER

            if(remLower) password = password.ascii_down();

            //REMOVE UPPER

            if(remUpper) password = password.ascii_up();

            //ONLY PIN

            if(onlyPin) {
                password = password.ascii_up();
                password = password.replace("+", "0");
                password = password.replace("/", "1");
                password = password.replace("=", "2");
                password = password.replace("A", "3");
                password = password.replace("B", "4");
                password = password.replace("C", "5");
                password = password.replace("D", "6");
                password = password.replace("E", "7");
                password = password.replace("F", "8");
                password = password.replace("G", "9");
                password = password.replace("H", "0");
                password = password.replace("I", "1");
                password = password.replace("J", "2");
                password = password.replace("K", "3");
                password = password.replace("L", "4");
                password = password.replace("M", "5");
                password = password.replace("N", "6");
                password = password.replace("O", "7");
                password = password.replace("P", "8");
                password = password.replace("Q", "9");
                password = password.replace("R", "0");
                password = password.replace("S", "1");
                password = password.replace("T", "2");
                password = password.replace("U", "3");
                password = password.replace("V", "4");
                password = password.replace("W", "5");
                password = password.replace("X", "6");
                password = password.replace("Y", "7");
                password = password.replace("Z", "8");
            }
            //CREATION QSTRING AVEC POINTS

            passwordQMask = "";


            int nbMask=1;
            if(length>7) nbMask=2;
            if(length>10) nbMask=3;
            for(int i=0; i<length; i++){
                if(i<nbMask || i>=length-nbMask){
                    passwordQMask += password.substring(i,1);
                }
                else{
                    passwordQMask += "\u25CF";
                }
            }

            // sortie[0] : password => mot de passe final
            // sortie[1] : password ou passwordQMask => mot de passe à afficher

            sortie[0] = password;
            //if(!config) passwordCourt = password;
            if(!mask) sortie += passwordQMask;
            if(mask) sortie += password;


        }

        return sortie;
    }

    private string calculMask(bool mask, string passwordCourt){

        string output = "";

        if(!mask) output = passwordQMask;
        if(mask) output = passwordCourt;

        return output;
    }
}
