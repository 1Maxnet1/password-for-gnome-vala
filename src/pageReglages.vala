namespace Password {
    [GtkTemplate (ui = "/org/emilien/Password/ui/pageReglages.ui")]
    public class Reglages : Adw.PreferencesWindow {
        [GtkChild]
        public unowned Adw.ActionRow actionPwned;
        [GtkChild]
        public unowned Adw.ComboRow comboHachage;
        [GtkChild]
        public unowned Gtk.Switch switchConversion;
        [GtkChild]
        public unowned Adw.ActionRow actionSalt;
        [GtkChild]
        public unowned Gtk.Entry texteSalage;
        [GtkChild]
        public unowned Adw.ActionRow actionAlias;
        [GtkChild]
        public unowned Gtk.Entry preAlias;
        [GtkChild]
        public unowned Gtk.Switch switchMasqueAlias;
        [GtkChild]
        public unowned Adw.ActionRow actionLength;
        [GtkChild]
        public unowned Gtk.SpinButton scaleLongueur;
        [GtkChild]
        public unowned Gtk.Switch switchMinuscules;
        [GtkChild]
        public unowned Gtk.Switch switchMajuscules;
        [GtkChild]
        public unowned Gtk.Switch switchChiffres;
        [GtkChild]
        public unowned Gtk.Switch switchPin;
        [GtkChild]
        public unowned Gtk.Switch switchSpeciaux;
        [GtkChild]
        public unowned Gtk.Switch switchEnablePwned;
        [GtkChild]
        public unowned Gtk.Switch switchPwned;
        [GtkChild]
        public unowned Gtk.Switch switchQuitter;
        [GtkChild]
        public unowned Gtk.Switch switchSuppression;
        [GtkChild]
        public unowned Gtk.SpinButton scaleTemps;
        [GtkChild]
        public unowned Gtk.Adjustment temps;
        [GtkChild]
        public unowned Gtk.Switch switchNotifications;
        [GtkChild]
        public unowned Gtk.Button boutonSupprimerProfil;
        [GtkChild]
        public unowned Adw.ActionRow actionSupProfil;
        [GtkChild]
        public unowned Gtk.Switch switchVerrouillage;
        [GtkChild]
        public unowned Gtk.Label labelForce;
        [GtkChild]
        public unowned Gtk.Label barre0;
        [GtkChild]
        public unowned Gtk.Label barre1;
        [GtkChild]
        public unowned Gtk.Label barre2;
        [GtkChild]
        public unowned Gtk.Label barre3;
        [GtkChild]
        public unowned Gtk.Label barre4;
        [GtkChild]
        public unowned Gtk.Image iconeBouclier;

        private bool lecture;

        public Window win {get; set;}

        public Widgets.Menu menu {get;set;}

        public Calculateur calculateur {get;set;}

        public Reglages (Window win, Widgets.Menu menu, Calculateur calculateur) {
            Object (
                win: win,
                menu: menu,
                calculateur: calculateur
            );

            transient_for = win;
            show ();

            texteSalage.icon_press.connect(() => {texteSalage.set_text("");});
            preAlias.icon_press.connect(() => {preAlias.set_text("");});
            preAlias.changed.connect(() => { calculateur.texteAlias.set_text(preAlias.get_text()); });
            switchMasqueAlias.notify["active"].connect(() => { calculateur.texteAlias.set_visibility(!switchMasqueAlias.get_active()); });

            comboHachage.notify["selected"].connect(() => {parametres[0] = (int32) comboHachage.get_selected(); chgtReglages();});
            switchConversion.notify["active"].connect(() => {parametres[1] = switchConversion.get_active(); chgtReglages();});
            texteSalage.changed.connect(() => {parametres[2] = texteSalage.get_text(); chgtReglages();});
            preAlias.changed.connect(() => {parametres[3] = preAlias.get_text(); chgtReglages();});
            scaleLongueur.value_changed.connect(() => {parametres[4] = (int) scaleLongueur.get_value(); chgtReglages();});
            switchMinuscules.notify["active"].connect(basculSwitchMinuscules);
            switchMajuscules.notify["active"].connect(basculSwitchMajuscules);
            switchChiffres.notify["active"].connect(basculSwitchChiffres);
            switchPin.notify["active"].connect(basculSwitchPin);
            switchSpeciaux.notify["active"].connect(() => {parametres[9] = switchSpeciaux.get_active(); chgtReglages();});

            switchNotifications.notify["active"].connect(() => {if(switchNotifications.get_active()) {avertissement("notifications");}});
            switchSuppression.notify["active"].connect(() => {if(switchSuppression.get_active()) {avertissement("clipboard");}});

            switchEnablePwned.notify["active"].connect(() =>{
                actionPwned.set_sensitive(switchEnablePwned.get_active());
                win.boutonPwned.set_visible(!switchPwned.get_active() && switchEnablePwned.get_active());
            });

            switchPwned.notify["active"].connect(() => {
                win.boutonPwned.set_visible(!switchPwned.get_active() && switchEnablePwned.get_active());
            });

            forceMotDePasse(this);
            if(win.profilActuel=="") actionSupProfil.hide();
            if(win.profilActuel!="") {
                actionSupProfil.set_title(_("Delete the profile") + " <b>" + win.profilActuel + "</b>");
                boutonSupprimerProfil.clicked.connect(suppressionProfil);
            }

            if(win.codeVerrouillage!="") switchVerrouillage.set_active(true);

            switchVerrouillage.notify["active"].connect(() => {
                if(switchVerrouillage.get_active()) fenetreVerrouillage();
                else deverrouillage();
            });

            this.close_request.connect( () => {win.chgtReglages();return false;});
        }

        construct {
            comboHachage.set_selected((uint) parametres[0].get_int32());
            switchConversion.set_active(parametres[1].get_boolean());
            texteSalage.set_text(parametres[2].get_string());
            if(parametres[2].get_string()!="") texteSalage.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            preAlias.set_text(parametres[3].get_string());
            if(parametres[3].get_string()!="") preAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            scaleLongueur.set_value(parametres[4].get_int32());
            switchMinuscules.set_active(parametres[5].get_boolean());
            switchMajuscules.set_active(parametres[6].get_boolean());
            switchChiffres.set_active(parametres[7].get_boolean());
            switchPin.set_active(parametres[8].get_boolean());
            switchSpeciaux.set_active(parametres[9].get_boolean());

            settings.bind("masquagealias", switchMasqueAlias, "active", DEFAULT);
            settings.bind("enablepwn", switchEnablePwned, "active", DEFAULT);
            settings.bind("pwn", switchPwned, "active", DEFAULT);
            settings.bind("quitter", switchQuitter, "active", DEFAULT);
            settings.bind("suppression", switchSuppression, "active", DEFAULT);
            settings.bind("delai", temps, "value", DEFAULT);
            settings.bind("notifications", switchNotifications, "active", DEFAULT);

            comboHachage.set_subtitle_lines (1);
            actionSalt.set_subtitle_lines (1);
            actionAlias.set_subtitle_lines (1);
            actionLength.set_subtitle_lines (1);
            texteSalage.set_size_request (100, -1);
            preAlias.set_size_request (100, -1);

            actionPwned.set_sensitive(false);

            if(settings.get_boolean("enablepwn")) actionPwned.set_sensitive(true);
        }

        private void basculSwitchMinuscules()
        {
            parametres[5] = switchMinuscules.get_active();
            if(switchMinuscules.get_state() && switchMajuscules.get_state()){
                switchMajuscules.set_active(false);
            }
            else{
                chgtReglages();
            }
        }

        private void basculSwitchMajuscules()
        {
            parametres[6] = switchMajuscules.get_active();
            if(switchMajuscules.get_state() && switchMinuscules.get_state()){
                switchMinuscules.set_active(false);
            }
            else{
                chgtReglages();
            }
        }

        private void basculSwitchChiffres()
        {
            parametres[7] = switchChiffres.get_active();
            if(switchChiffres.get_state() && switchPin.get_state()){
                switchPin.set_active(false);
            }
            else{
                chgtReglages();
            }
        }

        private void basculSwitchPin()
        {
            parametres[8] = switchPin.get_active();
            if(switchPin.get_state() && switchChiffres.get_state()){
                switchChiffres.set_active(false);
            }
            else{
                chgtReglages();
            }
        }

        private void chgtReglages() {

            if(win.codeVerrouillage == "") ecritureFichierConfig(win.profilActuel, win.dossierConfig);
            if(win.codeVerrouillage != "") ecritureFichierCryptConfig(win.profilActuel, win.dossierConfig, win.codeVerrouillage);

            if(preAlias.get_text()!="" && preAlias.get_icon_name(SECONDARY)==null) preAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            if(preAlias.get_text()=="" && preAlias.get_icon_name(SECONDARY)!=null) preAlias.set_icon_from_icon_name(SECONDARY,null);
            if(texteSalage.get_text()!="" && texteSalage.get_icon_name(SECONDARY)==null) texteSalage.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            if(texteSalage.get_text()=="" && texteSalage.get_icon_name(SECONDARY)!=null) texteSalage.set_icon_from_icon_name(SECONDARY,null);

            forceMotDePasse(this);
        }

        private void avertissement(string param){
            string fichier ="/.initWarn";
            if(param=="notifications") fichier+="Notif";
            if(param=="clipboard") fichier+="Clip";
            string message =_("This feature may not work on your system");
            if(!FileUtils.test(win.dossierConfig+fichier, FileTest.EXISTS)){
                Adw.MessageDialog dialog = new Adw.MessageDialog(this, message, "");
                dialog.add_response("ok", _("OK"));
                dialog.present();
                GLib.FileUtils.set_contents(win.dossierConfig+fichier, "");
            }
        }

        private void suppressionProfil()
        {
            //AVERTISSEMENT
            Adw.MessageDialog dialog = new Adw.MessageDialog(this, _("Are you sure you want to delete the profile "), win.profilActuel);
            dialog.close_response = "cancel";
            dialog.add_response("cancel", _("Cancel"));
            dialog.add_response("delete", _("Delete"));
            dialog.set_response_appearance("delete", Adw.ResponseAppearance.DESTRUCTIVE);

            dialog.response.connect( (response) => {
                if (response == "delete"){
                    FileUtils.remove(win.dossierConfig + "/password-" + win.profilActuel + ".conf");
                    menu.majListeProfils(false);
                    //ON CHARGE LE PREMIER PROFIL DE LA LISTE SI LISTE NON VIDE"
                    if(win.nbProfils==0) {
                        menu.chargementProfil("");
                        FileUtils.remove (win.dossierConfig + "/profile.conf");
                    }
                    if(win.nbProfils >0) menu.chargementProfil(win.listeProfils.nth_data(0));
                    //SUPPRESSION FICHIER DERNIER PROFIL CHARGÉ
                    this.close();
                }
            });

            dialog.present();
        }

        private void fenetreVerrouillage() {

            Adw.MessageDialog dialog = new Adw.MessageDialog(this, _("Lock settings"), _("Enter a 6 digits code\nThis code will be needed to access settings page"));
            dialog.close_response = "cancel";
            dialog.add_response("cancel", _("Cancel"));
            dialog.add_response("lock", _("Lock"));
            dialog.set_response_appearance("lock", Adw.ResponseAppearance.SUGGESTED);
            dialog.set_response_enabled ("lock", false);

            Gtk.Box boite = new Gtk.Box(Gtk.Orientation.VERTICAL,3);

            Adw.PreferencesGroup groupe = new Adw.PreferencesGroup();
            Adw.PasswordEntryRow entreeCode_1 = new Adw.PasswordEntryRow();
            Adw.PasswordEntryRow entreeCode_2 = new Adw.PasswordEntryRow();

            entreeCode_1.set_alignment((float) 0.5);
            entreeCode_2.set_alignment((float) 0.5);
            entreeCode_1.set_title ("Enter pin code");
            entreeCode_2.set_title ("Confirm pin code");

            Gtk.Button bouton = new Gtk.Button();
            bouton.set_label(_("Generate a random pin code..."));
            bouton.get_style_context().add_class("suggested-action");
            bouton.set_margin_start(20);
            bouton.set_margin_end(20);

            Gtk.ProgressBar barre = new Gtk.ProgressBar();
            barre.set_margin_top(-5);
            barre.set_margin_start(25);
            barre.set_margin_end(25);
            barre.set_visible(false);

            Gtk.Image icone = new Gtk.Image();
            icone.set_margin_top(15);
            icone.set_from_icon_name("dialog-warning-symbolic");
            Gtk.Label labelInfoLock = new Gtk.Label(_("This program uses the username, hostname and machine-id to generate a unique id to encrypt key and settings files. If one of these parameters is changed, the lock code will be unreadable and settings will need to be reset."));
            labelInfoLock.set_wrap(true);

            Pango.AttrList attrs = new Pango.AttrList ();
            attrs.insert (Pango.attr_weight_new (LIGHT));
            attrs.insert (Pango.attr_scale_new (0.8));
            labelInfoLock.set_attributes(attrs);
            labelInfoLock.set_hexpand(false);
            labelInfoLock.set_margin_top(5);

            groupe.add(entreeCode_1);
            groupe.add(entreeCode_2);
            boite.append(groupe);

            groupe.set_margin_bottom(5);

            boite.append(bouton);
            boite.append(barre);
            boite.append(icone);
            boite.append(labelInfoLock);

            dialog.set_extra_child(boite);

            //CONNEXIONS : /!\ RAJOUTER LA GÉNÉRATION DE CODE
            entreeCode_1.entry_activated.connect(() => {entreeCode_2.grab_focus();});
            entreeCode_1.changed.connect(() => {
                if(entreeCode_1.get_text().length > 6) {
                    GLib.Timeout.add (0, () => {
                        entreeCode_1.set_text(entreeCode_1.get_text().substring(0,6));
                        entreeCode_1.set_position(6);
                        return false;
                    });
                }
                if(entreeCode_1.get_text()!="" && entreeCode_2.get_text()!="") {
                    if(test6digits(entreeCode_1.get_text(),entreeCode_2.get_text())) dialog.set_response_enabled ("lock", true);
                    else dialog.set_response_enabled ("lock", false);
                }
            });

            bouton.clicked.connect(() => {
                if(!barre.is_visible()){
                    entreeCode_1.set_text(mdpAleatoire(true, 6, true,false,false,false,false,true,"")[1]);
                    entreeCode_2.set_text(entreeCode_1.get_text());
                    bouton.set_label(entreeCode_1.get_text());
                    barre.set_visible(true);
                    icone.set_margin_top(4);
                    GLib.Timeout.add (5000, () => {
                        bouton.set_label(_("Generate a random pin code..."));
                        barre.set_visible(false);
                        icone.set_margin_top(15);
                        return false;
                    });

                    for(int i=0;i<500;i++) {
                        double niveau = (double) i/500;
                        int temps = i*10;
                        GLib.Timeout.add(temps, () => {barre.set_fraction(niveau);return false;});
                    }
                }
            });

            entreeCode_2.changed.connect(() => {
                if(entreeCode_2.get_text().length > 6) {
                    GLib.Timeout.add (0, () => {
                        entreeCode_2.set_text(entreeCode_2.get_text().substring(0,6));
                        entreeCode_2.set_position(6);
                        return false;
                    });
                }
                if(entreeCode_1.get_text()!="" && entreeCode_2.get_text()!="") {
                    if(test6digits(entreeCode_1.get_text(),entreeCode_2.get_text())) dialog.set_response_enabled ("lock", true);
                    else dialog.set_response_enabled ("lock", false);
                }
            });
            entreeCode_2.entry_activated.connect(() => {
                if(dialog.get_response_enabled("lock")) {
                    verrouillage(entreeCode_1.get_text());
                    dialog.close_response = "lock";
                    dialog.close();
                }
            });

            dialog.response.connect((response) => {
                if(response=="lock") {
                    verrouillage(entreeCode_1.get_text());
                }
                else {
                    switchVerrouillage.set_active(false);
                }
            });

            dialog.present();
        }

        private bool entreeCodeVerrouillage(string code_1, string code_2) {

            bool test = false;
            if(code_1.char_count() == 6 && code_2.char_count() == 6 && code_1 == code_2){
                bool testNum = true;
                //VERIFICATION QUE CODE1 EST COMPOSÉ UNIQUEMENT DE CHIFFRE
                for(int i=0;i<6;i++){
                    if(!code_1.get_char(i).isdigit() && testNum) testNum = false;
                }
                if(testNum) test = true;
            }

            return test;
        }

        private void verrouillage(string code) {

            win.codeVerrouillage = code;
            ecritureCle(win.dossierConfig, win.cle1, win.cle2, win.codeVerrouillage, win.uuid);
            ecritureFichierCryptConfig(win.profilActuel, win.dossierConfig, win.codeVerrouillage);//REECRITURE FICHIER CONFIG CHIFFRÉ
            if(win.listeProfils.length() > 0) chiffrementProfils(win.listeProfils,win.profilActuel,win.dossierConfig,win.codeVerrouillage);
        }

        private void deverrouillage() {

            if(win.listeProfils.length() > 0) dechiffrementProfils(win.listeProfils,win.profilActuel,win.dossierConfig,win.codeVerrouillage);
            win.codeVerrouillage = "";
            FileUtils.remove(win.dossierConfig + "/cryptKey.conf");
            ecritureFichierConfig(win.profilActuel, win.dossierConfig);//REECRITURE FICHIER CONFIG EN CLAIR
        }
    }
}

