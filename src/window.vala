/* window.vala
 *
 * Copyright 2020-2022 Emilien
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Password {

    private GLib.Variant[] parametres;
	[GtkTemplate (ui = "/org/emilien/Password/ui/window.ui")]
	public class Window : Adw.ApplicationWindow {

        private const GLib.ActionEntry[] ACTION_ENTRIES = {
            //{ "monitor", basculMonitor },
            { "preferences", on_preferences_action },
            { "reinitialisation", reInitialisation},
        };

		//WIDGETS
        [GtkChild] private unowned Adw.HeaderBar headerbar;
        //[GtkChild] private unowned Gtk.Label titre;
        //[GtkChild] public unowned Gtk.Label sousTitre;
		//[GtkChild] public unowned Gtk.Stack fenetre;
		[GtkChild] private unowned Gtk.Revealer revealBarre;
		[GtkChild] private unowned Gtk.Revealer revealHaut;
		[GtkChild] private unowned Gtk.Revealer revealBas;
		[GtkChild] private unowned Gtk.Box barre;
        [GtkChild] private unowned Gtk.Box barreBas;
		[GtkChild] private unowned Gtk.Box barreHaut;
		//[GtkChild] private unowned Gtk.Box barreBouton;

        [GtkChild] public unowned Gtk.Button boutonNettoyer;
		[GtkChild] public unowned Gtk.Revealer revealCopier;
		[GtkChild] private unowned Gtk.Button boutonCopier;
		[GtkChild] public unowned Gtk.Button boutonPwned;
		[GtkChild] public unowned Gtk.Switch switchMask;
		[GtkChild] public unowned Gtk.Image iconeSwitchMask;
		[GtkChild] public unowned Gtk.MenuButton boutonMenu;
		//[GtkChild] private unowned WebKit.WebView web_view;
		//[GtkChild] private unowned Gtk.Revealer revealVerrouillage;
		//[GtkChild] private unowned Gtk.Switch switchVerrouillage;
		//[GtkChild] private unowned Gtk.Image imageVerrouillage;
		[GtkChild] private unowned Adw.ToastOverlay toast_overlay;
		[GtkChild] private unowned Gtk.Viewport vueCalculateur;
        [GtkChild] private unowned Gtk.Viewport vueGenerateur;
        [GtkChild] private unowned Gtk.Viewport vueMonitor;
        [GtkChild] public unowned Adw.ViewStack stack;
        [GtkChild] public unowned Adw.ViewStackPage stackPageCalculator;
        [GtkChild] public unowned Adw.ViewStackPage stackPageGenerator;
        [GtkChild] public unowned Adw.ViewStackPage stackPageMonitor;
        [GtkChild] private unowned Adw.ViewSwitcherBar barreSwitch;

        public Widgets.Menu menu;
        public Calculateur calculateur;
        public Generateur generateur;
        public Reglages reglages;
        public Monitor monitor;
        //public Web web;

		//FIN WIDGETS

        public string dossierConfig;
        public string profilActuel;
        public string cle1;
        public string cle2;
        public string codeVerrouillage;
        public bool verrou;
        public string uuid;
        public List<string> listeProfils;
        public int nbProfils;
        public bool suppressionProfil;
        public bool quitterAppli;
        public bool attendreAvanDeFermer;
        private int largeur;
        private bool phosh;
        public string randomPassword;
        public string pass;
        public string passwordCourt;

        public Window (Gtk.Application app) {

			Object (application: app);
			add_action_entries (ACTION_ENTRIES, this);

            this.set_hide_on_close(true);

            //INITIALISATION VARIABLES
            codeVerrouillage = "";
            passwordCourt = "";
            randomPassword = "";
            pass = "";
            dossierConfig = "";
            suppressionProfil=false;
            quitterAppli=false;
            attendreAvanDeFermer=false;
            phosh=false;
            largeur=-1;
            nbProfils = 0;
            profilActuel = "";
            verrou = false;

            //LECTURE CLÉ UNIQUE

            cle1 = " ";
            cle2 = " ";
            uuid = " ";

            string? user = Environment.get_user_name();
            string? host = Environment.get_host_name();

            string fichierMachine = "/etc/machine-id";
            if(File.new_for_path(fichierMachine).query_exists()){
                var dis = FileStream.open (fichierMachine, "r");
                uuid = dis.read_line();
            }

            if(user.char_count()!=0) cle1 = user;
            if(host.char_count()!=0) cle2 = host;
            if(uuid.char_count()==0) uuid = " ";

            //CONFIGURATION
            //VERSION 1.0.9 : DÉPLACEMENT FICHIERS CONFIG VERS CONFIG/PASSWORD/.
            dossierConfig = Environment.get_user_config_dir()+"/password";
            if(!FileUtils.test(dossierConfig, FileTest.IS_DIR)) DirUtils.create(dossierConfig,0700);
            if(FileUtils.test(dossierConfig+"/../key.conf", FileTest.EXISTS) && !FileUtils.test(dossierConfig+"/key.conf", FileTest.EXISTS)){
                FileUtils.rename(dossierConfig+"/../key.conf", dossierConfig+"/key.conf");
            }
            if(FileUtils.test(dossierConfig+"/../cryptKey.conf", FileTest.EXISTS) && !FileUtils.test(dossierConfig+"/cryptKey.conf", FileTest.EXISTS)){
                FileUtils.rename(dossierConfig+"/../cryptKey.conf", dossierConfig+"/cryptKey.conf");
            }
            if(FileUtils.test(dossierConfig+"/../password.conf", FileTest.EXISTS) && !FileUtils.test(dossierConfig+"/password.conf", FileTest.EXISTS)){
                FileUtils.rename(dossierConfig+"/../password.conf", dossierConfig+"/password.conf");
            }
            if(FileUtils.test(dossierConfig+"/../theme.conf", FileTest.EXISTS) && !FileUtils.test(dossierConfig+"/theme.conf", FileTest.EXISTS)){
                FileUtils.rename(dossierConfig+"/../theme.conf", dossierConfig+"/theme.conf");
            }

            initialisationParametres();

            //MAJ PROFILS + CREATION MENU
            menu.majListeProfils(true);
            //DERNIER PROFIL CHARGÉ
            calculateur.labelProfil.hide();
            generateur.labelProfil.hide();
            if(nbProfils > 0){
                string fichierProfil = dossierConfig+"/profile.conf";
                if(File.new_for_path(fichierProfil).query_exists()){
                    var dis = FileStream.open (fichierProfil, "r");
                    string profil = dis.read_line();
                    bool profilExiste = false;
                    for(int i=0;i<nbProfils;i++){
                        if(listeProfils.nth_data((uint) i) == profil) profilExiste = true;
                    }
                    if(profilExiste) menu.chargementProfil(profil);
                }
            }

            lectureConfig();

            //DIMENSIONS FENETRE

            this.set_default_size(settings.get_int("largeur"), settings.get_int("hauteur"));

            largeur = get_allocated_width();

            boutonPwned.hide();
            if(settings.get_boolean("enablepwn") && !settings.get_boolean("pwn")) boutonPwned.show();
            calculateur.texteAlias.set_visibility(!settings.get_boolean("masquagealias"));

            GLib.Timeout.add (500, () => {
                if(!barreSwitch.get_reveal()) phosh=true;
                chgtTaille(true);
                notify.connect(() => {chgtTaille(false);});
                return false;
            });

            if(settings.get_boolean("masquage")) iconeSwitchMask.set_from_icon_name("view-reveal-symbolic");

			//CONNEXIONS

            //notify.connect(chgtTaille);
            hide.connect(quitter);
            this.close_request.connect(() => {//ecritureFichierDim();return false;});
                settings.set_int("largeur", get_width());
                settings.set_int("hauteur", get_height());
                return false;
            });

            switchMask.notify["active"].connect(basculMask);
            boutonNettoyer.clicked.connect(nettoyer);
            boutonCopier.clicked.connect(copier);

            boutonPwned.clicked.connect(() => {
                if(stack.get_visible_child_name()=="calculateur"){
                    GLib.Timeout.add (100, () => {calculateur.majFaille();return false;});
                }
                else if(stack.get_visible_child_name()=="generateur") {
                    GLib.Timeout.add (100, () => {generateur.majFailleAleatoire();return false;});
                };
            });

            if(settings.get_int("onglet") == 0) stack.set_visible_child_name("calculateur");
            if(settings.get_int("onglet") == 1) stack.set_visible_child_name("generateur");
            if(settings.get_int("onglet") == 2) stack.set_visible_child_name("monitor");

            stack.notify.connect(() => {
                if(stack.get_visible_child_name()=="calculateur") basculModeCalculateur();
                else if(stack.get_visible_child_name()=="generateur") basculModeGenerateur();
                else if(stack.get_visible_child_name()=="monitor") basculMonitor();
            });

            GLib.Timeout.add (500, () => {avertissement();return false;});
		}

		construct {
            //AJOUT PAGES
            generateur = new Generateur(this);
            calculateur = new Calculateur(this, generateur);
            monitor = new Monitor(this);
            //web = new Web(this);

            //MENU
            menu = new Widgets.Menu(this, calculateur, generateur);

            vueCalculateur.set_child(calculateur);
            vueGenerateur.set_child(generateur);
            vueMonitor.set_child(monitor);

            settings.bind ("masquage",
                switchMask, "active",
                DEFAULT
            );
        }

		//SUBROUTINES

        private void basculMask() {
            if(switchMask.get_active()==true) {
                switchMask.set_tooltip_text(_("Mask password"));
                iconeSwitchMask.set_from_icon_name("view-reveal-symbolic");
            }
            else {
                iconeSwitchMask.set_from_icon_name("view-conceal-symbolic");
                switchMask.set_tooltip_text(_("Unmask password"));
            }

            if(calculateur.texteAlias.get_text()!="" && calculateur.texteSecret.get_text()!="") calculateur.motDePasse.set_label(calculMask(switchMask.get_state(), passwordCourt));
            if(generateur.motDePasseAleatoire.get_text()!="") generateur.motDePasseAleatoire.set_label(mdpAleatoire(false, parametres[4].get_int32(), settings.get_boolean("masquage"),//
                parametres[5].get_boolean(), parametres[6].get_boolean(), parametres[7].get_boolean(),//
                parametres[9].get_boolean(), parametres[8].get_boolean(), randomPassword )[2]);
        }

        private void basculModeCalculateur() {

            if(settings.get_int("onglet")!=0) {
                settings.set_int("onglet",0);
                if(barreSwitch.get_reveal() && !revealHaut.get_reveal_child()) revealHaut.set_reveal_child(true);//MODE TELEPHONE
                else if(!barreSwitch.get_reveal() && !revealBarre.get_reveal_child()) revealBarre.set_reveal_child(true); //MODE PC
                if(passwordCourt=="" && revealCopier.get_child_revealed()) revealCopier.set_reveal_child(false);
                if(passwordCourt!="" && !revealCopier.get_child_revealed()) revealCopier.set_reveal_child(true);
            }
        }

        private void basculModeGenerateur() {

            if(settings.get_int("onglet")!=1) {
                settings.set_int("onglet",1);
                if(barreSwitch.get_reveal() && !revealHaut.get_reveal_child()) revealHaut.set_reveal_child(true);//MODE TELEPHONE
                else if(!barreSwitch.get_reveal() && !revealBarre.get_reveal_child()) revealBarre.set_reveal_child(true); //MODE PC
                if(randomPassword=="" && revealCopier.get_child_revealed()) revealCopier.set_reveal_child(false);
                if(randomPassword!="" && !revealCopier.get_child_revealed()) revealCopier.set_reveal_child(true);
            }
        }

        private void basculMonitor() {

            if(settings.get_int("onglet")!=2) {
                settings.set_int("onglet",2);
                if(barreSwitch.get_reveal() && revealHaut.get_reveal_child()) revealHaut.set_reveal_child(false);//MODE TELEPHONE
                else if(!barreSwitch.get_reveal() && revealBarre.get_reveal_child()) revealBarre.set_reveal_child(false); //MODE PC
            }



            //Gtk.show_uri(null, url, Gdk.CURRENT_TIME); //PROVISOIRE // EN ATTENTE DE WEBKITGTK6
            /*
            if(!phosh) revealHaut.set_reveal_child(false);
            if(phosh) revealBarre.set_reveal_child(false);
            revealVerrouillage.set_reveal_child(false);
            //if(modeSombre.get_active()) iconeBoutonMenuMode.set_from_icon_name("firefox_monitor", BUTTON);
            //if(!modeSombre.get_active()) iconeBoutonMenuMode.set_from_icon_name("firefox_monitor_light", BUTTON);
            titre.set_text(_("Firefox Monitor"));
            //fenetre.set_visible_child(web_view);
            revealCopier.set_reveal_child(false);
            //web_view.load_uri(url);
            */
        }

        private void basculPwn() {


            //if(!phosh) revealHaut.set_reveal_child(false);
            //if(phosh) revealBarre.set_reveal_child(false);
            //revealVerrouillage.set_reveal_child(false);
            //if(Adw.StyleManager.get_default().get_dark()) boutonMenuMode.set_icon_name("iconePwned");
            //if(!Adw.StyleManager.get_default().get_dark()) boutonMenuMode.set_icon_name("iconePwned_light");
            //sousTitre.set_text("Password check");
            //fenetre.set_visible_child(pwned);
            revealCopier.set_reveal_child(false);
        }

        private void on_preferences_action() {

            if(codeVerrouillage=="" || !verrou) new Reglages (this, menu, calculateur);
            else fenetreDeverrouillage();
        }

        private void fenetreDeverrouillage() {

            Adw.MessageDialog dialog = new Adw.MessageDialog(this, _("Settings locked"), _("Enter code to unlock"));
            dialog.close_response = "cancel";
            dialog.add_response("cancel", _("Cancel"));

            Gtk.Box boite = new Gtk.Box(Gtk.Orientation.VERTICAL,3);

            Adw.PreferencesGroup groupe = new Adw.PreferencesGroup();
            Adw.PasswordEntryRow entreeCode = new Adw.PasswordEntryRow();
            entreeCode.set_title(_("Enter your pin code"));
            entreeCode.set_alignment((float) 0.5);

            Gtk.Label labelEntreeCodeIncorrect = new Gtk.Label(_("Incorrect code"));
            labelEntreeCodeIncorrect.set_margin_top(5);
            labelEntreeCodeIncorrect.hide();
            var css = new Gtk.CssProvider();
            uint8[] style = "* { color: #ca0000; }".data;
            css.load_from_data(style);
            labelEntreeCodeIncorrect.get_style_context().add_provider(css, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

            groupe.add(entreeCode);
            boite.append(groupe);
            boite.append(labelEntreeCodeIncorrect);

            dialog.set_extra_child(boite);

            entreeCode.changed.connect(() => {
                if(entreeCode.get_text().length>=6) {
                    entreeCode.set_editable(false);
                    if(test6digits(entreeCode.get_text(),entreeCode.get_text()) && entreeCode.get_text()==codeVerrouillage) {
                        dialog.close();
                        verrou=false;
                        new Reglages (this, menu, calculateur);
                    }
                    else {
                        labelEntreeCodeIncorrect.show();
                        for(int i=1;i<=6;i++) {
                            string anim = "******".slice(0,6-i);
                            GLib.Timeout.add (i*50, () => {entreeCode.set_text(anim);return false;});
                        }
                        GLib.Timeout.add (3000, () => {labelEntreeCodeIncorrect.hide();return false;});
                    }
                    entreeCode.set_editable(true);
                }
            });

            dialog.present();
        }

        public void chgtReglages() {

            if(passwordCourt!=""){
                string[] mdp = mdpCalcul(calculateur.texteAlias.get_text(), calculateur.texteSecret.get_text(), settings.get_boolean("masquage"),//
                    parametres[0].get_int32(), parametres[1].get_boolean(), parametres[4].get_int32(),//
                    parametres[2].get_string(), parametres[5].get_boolean(), parametres[6].get_boolean(),//
                    parametres[7].get_boolean(), parametres[9].get_boolean(), parametres[8].get_boolean(), false );
                calculateur.motDePasse.set_label(mdp[1]);
                passwordCourt = mdp[0];
                if(settings.get_boolean("pwn")) GLib.Timeout.add (100, () => {calculateur.majFaille();return false;});
                else{calculateur.labelFaille.set_label("");}
            }
            if(randomPassword!=""){
                generateur.motDePasseAleatoire.set_label(mdpAleatoire(false, parametres[4].get_int32(), settings.get_boolean("masquage"),//
                    parametres[5].get_boolean(), parametres[6].get_boolean(), parametres[7].get_boolean(),//
                    parametres[9].get_boolean(), parametres[8].get_boolean(), randomPassword )[2]); //ON CHANGE L'AFFICHAGE DU MOT DE PASSE ALEATOIRE
                if(settings.get_boolean("pwn")) GLib.Timeout.add (100, () => {generateur.majFailleAleatoire();return false;});
                else{generateur.labelFailleAleatoire.set_label("");}
            }
        }

        private void lectureConfig() {

            string fichierCle = dossierConfig + "/key.conf";
            string fichierCleChiffree = dossierConfig + "/cryptKey.conf";
            string fichierConfig = dossierConfig + "/password.conf";
            string fichierTheme = dossierConfig + "/theme.conf";

            if(File.new_for_path(fichierCle).query_exists()){
                codeVerrouillage = "";
                var dis = FileStream.open (fichierCle, "r");
                codeVerrouillage = dis.read_line();
                if(codeVerrouillage.char_count() == 6){
                    bool testNum = true;
                    //VERIFICATION CODE EST COMPOSÉ UNIQUEMENT DE CHIFFRE
                    for(int i=0;i<6;i++){
                        if(!codeVerrouillage.get_char(i).isdigit() && testNum) testNum = false;
                    }
                    if(testNum){
                        verrou = true;
                    }
                    else{
                        codeVerrouillage = "";
                        verrou = false;
                    }
                }
                //ANCIENNE CONFIG, ON CHIFFRE LA CLÉ
                ecritureCle(dossierConfig, cle1, cle2, codeVerrouillage, uuid);
                lectureFichierConfig(dossierConfig, profilActuel);
                ecritureFichierCryptConfig(profilActuel, dossierConfig, codeVerrouillage);
                FileUtils.remove(dossierConfig + "/key.conf");
            }

            if(File.new_for_path(fichierCleChiffree).query_exists()){
                //CREATION CLÉ DE CHIFFREMENT UNIQUE
                string cle = mdpCalcul(cle1,cle2, true, 4, true, 12 ,uuid, false , false , false , false , true, true)[0];
                //LECTURE CLÉ CHIFFRÉE
                codeVerrouillage = "";
                var dis = FileStream.open (fichierCleChiffree, "r");
                string cleChiffree = dis.read_line();
                for(int i=0;i<6;i++){
                    unichar ch = cleChiffree.get_char(cleChiffree.index_of_nth_char(i));
                    int32 crypt = (int32) int.parse(cle.slice(2*i,2*i+2));
                    ch -= crypt;
                    codeVerrouillage += ch.to_string();
                }

                verrou = true;
            }

            if(File.new_for_path(fichierConfig).query_exists()){
                if(codeVerrouillage == "") lectureFichierConfig(dossierConfig, profilActuel);
                if(codeVerrouillage != "") lectureFichierCryptConfig(dossierConfig, profilActuel, codeVerrouillage);
            }

            if(File.new_for_path(fichierTheme).query_exists()){
                var dis = FileStream.open (fichierTheme, "r");
                string theme = dis.read_line();
                if(theme == "DEFAULT") menu.follow.set_active(true);
                if(theme == "FORCE_LIGHT") menu.light.set_active(true);
                if(theme == "FORCE_DARK") menu.dark.set_active(true);
                menu.changeTheme(false);
            }
            else{
                menu.follow.set_active(true);
            }

            //PRE-REMPLISSAGE ALIAS

            if(parametres[3].get_string()!=""){
                calculateur.texteAlias.set_text(parametres[3].get_string());
                calculateur.texteSecret.grab_focus();
                calculateur.texteAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            }
        }

        private void initialisationParametres() {

            parametres += new GLib.Variant("i",0);          //0 : ALGO HACHAGE
            parametres += new GLib.Variant("b",false);      //1 : CONVERSION HEXA-->ASCII
            parametres += new GLib.Variant("s","");         //2 : SALAGE
            parametres += new GLib.Variant("s","");         //3 : ALIAS
            parametres += new GLib.Variant("i",12);         //4 : LONGUEUR
            parametres += new GLib.Variant("b",false);      //5 : MINUSCULES
            parametres += new GLib.Variant("b",false);      //6 : MAJUSCULES
            parametres += new GLib.Variant("b",false);      //7 : CHIFFRES
            parametres += new GLib.Variant("b",false);      //8 : PIN
            parametres += new GLib.Variant("b",false);      //9 : SPECIAUX
        }

        private void reInitialisation() {
            //AVERTISSEMENT
            Adw.MessageDialog confirmation;
            if(listeProfils.length()>0){
                confirmation = new Adw.MessageDialog(this, _("Are you sure you want to delete the settings and all the profiles?"), null);
            }
            else{
                confirmation = new Adw.MessageDialog(this, _("Are you sure you want to delete the settings?"), null);
            }
            confirmation.close_response = "cancel";
            confirmation.add_response("cancel", _("Cancel"));
            confirmation.add_response("delete", _("Delete"));
            confirmation.set_response_appearance("delete", Adw.ResponseAppearance.DESTRUCTIVE);

            confirmation.response.connect ((response) => {
                if (response == "delete"){

                    parametres.length = 0;
                    initialisationParametres();
                    calculateur.texteAlias.set_text("");
                    calculateur.texteSecret.set_text("");
                    settings.set_boolean("quitter",false);
                    settings.set_boolean("suppression",false);
                    settings.set_int("delai",10);
                    settings.set_boolean("notifications",false);
                    settings.set_boolean("enablepwn",false);
                    settings.set_boolean("pwn",false);
                    boutonPwned.hide();
                    settings.set_boolean("masquagealias",false);
                    settings.set_boolean("masquage",false);

                    codeVerrouillage="";
                    verrou = false;
                    FileUtils.remove(dossierConfig + "/password.conf");
                    FileUtils.remove(dossierConfig + "/cryptKey.conf");
                    FileUtils.remove(dossierConfig + "/key.conf");
                    FileUtils.remove(dossierConfig + "/profile.conf");
                    for(int i=0;i<listeProfils.length();i++){
                        FileUtils.remove(dossierConfig + "/password-" + listeProfils.nth_data((uint) i) + ".conf");
                    }
                    menu.majListeProfils(false);
                    profilActuel = "";
                }
            });

            confirmation.present();
        }

        private void quitter() {
            if(!attendreAvanDeFermer) get_application().quit();
            if(attendreAvanDeFermer) quitterAppli=true;
        }

        private void effacement() {
            clipboard("");

            if(settings.get_boolean("notifications")) envoiNotification(_("Password deleted"));

            attendreAvanDeFermer=false;
            if(quitterAppli) Timeout.add(1000, () => {quitter(); return false;});
        }

        private void nettoyer() {

            clipboard("");
            if(generateur.motDePasseAleatoire.get_label()=="" && calculateur.texteSecret.get_text()=="") calculateur.texteAlias.set_text("");
            calculateur.texteSecret.set_text("");
            passwordCourt="";
            randomPassword="";
            calculateur.motDePasse.set_label(_("your password"));
            generateur.motDePasseAleatoire.set_label("");
            calculateur.labelFaille.set_label("");
            generateur.labelFailleAleatoire.set_label("");
            revealCopier.set_reveal_child(false);
            boutonNettoyer.set_icon_name("user-trash-symbolic");
        }

        public void copier() {

            if(stack.get_visible_child_name()=="calculateur" && passwordCourt!="") clipboard(passwordCourt);
            if(stack.get_visible_child_name()=="generateur" && pass!="") clipboard(pass);
            if(settings.get_boolean("notifications")) envoiNotification(_("Password copied to the clipboard"));

            if(settings.get_boolean("suppression")){
                if(!quitterAppli) attendreAvanDeFermer=true;//AU CAS OU L'UTILISATEUR VEUILLE FERMER L'APPLI APRES AVOIR APPUYER SUR COPIE
                GLib.Timeout.add((uint) settings.get_int("delai")*1000, () => {Gdk.Display.get_default ().get_clipboard ().set_text (""); return false;});
                GLib.Timeout.add((uint) settings.get_int("delai")*1000, () => {effacement(); return false;});
            }

            if(quitterAppli) hide();
        }

        private void chgtTaille(bool init) {

            if((barreSwitch.get_reveal() || init) && !phosh) {//MODE PC => MODE TELEPHONE
                if(settings.get_int("onglet")!=2) revealHaut.set_reveal_child(true);
                revealBas.set_reveal_child(false);
                if(barre.get_parent()==barreBas) barreBas.remove(barre);
                barreHaut.append(barre);
                revealBarre.set_reveal_child(false);
                phosh=true;
            }
            else if((!barreSwitch.get_reveal() || init) && phosh) {//MODE TELEPHONE => MODE PC
                if(settings.get_int("onglet")!=2) revealBarre.set_reveal_child(true);
                revealBas.set_reveal_child(true);
                if(barre.get_parent()==barreHaut) barreHaut.remove(barre);
                barreBas.append(barre);
                revealHaut.set_reveal_child(false);
                phosh=false;
            }
        }

        private void envoiNotification(string id) {
            GLib.Notification notification = new Notification ("Password");
            notification.set_body (id);
            notification.set_priority (HIGH);
            notification.set_icon (new ThemedIcon("dialog-password"));
            var app = (Password.Application) GLib.Application.get_default ();
            app.send_notification ("org.emilien.Password", notification);
        }

        private void clipboard(string id) {
            string textNotif = _("Password copied");
            if(id=="") textNotif =_("Password cleared");
            Adw.Toast notif = new Adw.Toast(textNotif);
            toast_overlay.add_toast(notif);

            Gdk.Display.get_default ().get_clipboard ().set_text (id);
        }

        private void avertissement(){
            string fichier =dossierConfig+"/.initWarnData";

            if(!FileUtils.test(fichier, FileTest.EXISTS)){

                string message =_("Password does not collect any data. Generated and calculated passwords are only copied into the device clipboard at the user's request.\n\n");
                message +=_("The integrity password check does not send the password on the internet, but only the beginning of its SHA-1 hash. For more details, see the ");
                message +="<a href=\"https://haveibeenpwned.com/API/v3#PwnedPasswords\" title=\""+_("HaveIBeenPwned API...")+"\">"+_("HaveIBeenPwned API")+"</a>.\n\n";
                message += _("Do you want to keep this feature enabled?\n");
                message += _("You can always change this setting in the application preferences.");

                Adw.MessageDialog dialog = new Adw.MessageDialog(this, "", "");
                dialog.close_response = "ok";
                dialog.add_response("ok", _("Yes"));
                dialog.add_response("no", _("No"));
                dialog.set_response_appearance("ok", Adw.ResponseAppearance.SUGGESTED);
                dialog.set_response_appearance("no", Adw.ResponseAppearance.DESTRUCTIVE);

                Gtk.Label label = new Gtk.Label(null);
                label.set_wrap(true);
                label.set_markup(message);

                dialog.set_extra_child(label);

                dialog.response.connect((response) => {
                    if(response=="ok") settings.set_boolean("enablepwn", true);
                    if(response=="no") settings.set_boolean("enablepwn", false);
                });
                dialog.present();
                GLib.FileUtils.set_contents(fichier, "");
            }
        }
	}   //FIN WINDOW
}   //FIN NAMESPACE
